#include "MockedNetworkReply.h"
#include <QTimer>

struct QCustomNetworkReplyPrivate
{
    QByteArray content;
    qint64 offset;
};

MockedNeworkReply::MockedNeworkReply( QObject *parent )
    : QNetworkReply(parent)
{
    d = new QCustomNetworkReplyPrivate;
}

MockedNeworkReply::~MockedNeworkReply()
{
    delete d;
}

void MockedNeworkReply::setHttpStatusCode( int code, const QByteArray &statusText )
{
    setAttribute( QNetworkRequest::HttpStatusCodeAttribute, code );
    if ( statusText.isNull() )
        return;

    setAttribute( QNetworkRequest::HttpReasonPhraseAttribute, statusText );
}

void MockedNeworkReply::setHeader( QNetworkRequest::KnownHeaders header, const QVariant &value )
{
    QNetworkReply::setHeader( header, value );
}

void MockedNeworkReply::setContentType( const QByteArray &contentType )
{
    setHeader(QNetworkRequest::ContentTypeHeader, contentType);
}

void MockedNeworkReply::setContent( const QString &content )
{
    setContent(content.toUtf8());
}

void MockedNeworkReply::setContent( const QByteArray &content )
{
    d->content = content;
    d->offset = 0;

    open(ReadOnly | Unbuffered);
    setHeader(QNetworkRequest::ContentLengthHeader, QVariant(content.size()));

    QTimer::singleShot( 0, this, SIGNAL(readyRead()) );
    QTimer::singleShot( 0, this, SIGNAL(finished()) );
}

void MockedNeworkReply::abort()
{
    // NOOP
}


qint64 MockedNeworkReply::bytesAvailable() const
{
    return d->content.size() - d->offset + QIODevice::bytesAvailable();
}

bool MockedNeworkReply::isSequential() const
{
    return true;
}


qint64 MockedNeworkReply::readData(char *data, qint64 maxSize)
{
    if (d->offset >= d->content.size())
        return -1;

    qint64 number = qMin(maxSize, d->content.size() - d->offset);
    memcpy(data, d->content.constData() + d->offset, number);
    d->offset += number;

    return number;
}

