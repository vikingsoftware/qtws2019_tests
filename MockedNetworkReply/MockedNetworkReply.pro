QT -= gui
QT += network

CONFIG += c++11 console

SOURCES += \
    main.cpp \
    MockedNetworkReply.cpp \
    MockedNetworkAccessManager.cpp


HEADERS += \
    MockedNetworkReply.h \
    MockedNetworkAccessManager.h
