#ifndef MOCKEDNETWORKACCESSMANAGER_H
#define MOCKEDNETWORKACCESSMANAGER_H

#include <QNetworkAccessManager>


class MockedNetworkAccessManager : public QNetworkAccessManager
{
protected :

    QNetworkReply *createRequest(
            QNetworkAccessManager::Operation op,
            const QNetworkRequest &originalReq,
            QIODevice *outgoingData = nullptr) override;

};

#endif // MOCKEDNETWORKACCESSMANAGER_H
