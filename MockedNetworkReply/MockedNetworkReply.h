#ifndef MOCKNETWORKREPLY_H
#define MOCKNETWORKREPLY_H

#include <QNetworkReply>

class MockedNeworkReply : public QNetworkReply
{
    Q_OBJECT

public:
    MockedNeworkReply( QObject *parent= nullptr );
    ~MockedNeworkReply();

    void setHttpStatusCode( int code, const QByteArray &statusText = QByteArray() );
    void setHeader( QNetworkRequest::KnownHeaders header, const QVariant &value );
    void setContentType( const QByteArray &contentType );

    void setContent( const QString &content );
    void setContent( const QByteArray &content );

    void abort();
    qint64 bytesAvailable() const;
    bool isSequential() const;

protected:
    qint64 readData(char *data, qint64 maxSize);

private:
    struct QCustomNetworkReplyPrivate *d;
};

#endif // MOCKNETWORKREPLY_H
