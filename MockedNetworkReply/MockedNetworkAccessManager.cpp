#include "MockedNetworkAccessManager.h"

#include "MockedNetworkReply.h"

QNetworkReply *MockedNetworkAccessManager::createRequest(
        QNetworkAccessManager::Operation op,
        const QNetworkRequest &originalReq,
        QIODevice *outgoingData)
{
    Q_UNUSED(outgoingData)
    Q_UNUSED(op)

    MockedNeworkReply *reply = new MockedNeworkReply();
    reply->setHeader(QNetworkRequest::ContentTypeHeader, "text/json");
    reply->setContent(QString(
R"JSON({
    "message" : "haha!",
    "mocked"  : True
    "iAmNot"  : "%1"
})JSON"
).arg(originalReq.url().host()));
    reply->setHttpStatusCode( 200, "OK");
    return reply;

} ;
