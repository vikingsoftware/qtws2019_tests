#include <QCoreApplication>

#include "MockedNetworkAccessManager.h"
#include <QNetworkReply>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QNetworkAccessManager  *nam = new QNetworkAccessManager();

// Use this line instead of the one above to get a mocked network reply
//    MockedNetworkAccessManager *nam = new MockedNetworkAccessManager();
    QNetworkRequest request;
    request.setUrl(QUrl("https://most.likely.not.a.domain"));
    QNetworkReply * reply = nam->get(request);

    QObject::connect(reply, &QNetworkReply::finished, [&]()
    {
        qDebug() << reply->readAll().constData() ;
        a.exit();
    });

    QObject::connect(reply, QOverload<QNetworkReply::NetworkError>::of(&QNetworkReply::error),
        [&](QNetworkReply::NetworkError code)
    {
        qDebug() << "Request failed with code" << code;
        a.exit();
    });


    return a.exec();
}
